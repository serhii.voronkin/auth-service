<?php declare(strict_types=1);

namespace App\Infrastructure\Security\Exception;

use Exception;
use Throwable;

class UserTokenExpired extends Exception
{
    public function __construct($message = "User token expired", $code = 401, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
