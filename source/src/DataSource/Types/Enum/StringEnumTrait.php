<?php declare(strict_types=1);

namespace App\DataSource\Types\Enum;


use ReflectionClass;
use ReflectionException;
use UnexpectedValueException;

trait StringEnumTrait
{
    /**
     * @var string
     */
    protected string $value;

    /**
     * Enum constructor.
     *
     * @param $value
     *
     * @throws ReflectionException
     */
    public function __construct(string $value)
    {
        if (!in_array($value, $this->getConstants(), true)) {
            throw new UnexpectedValueException(sprintf('%s enum value does not exists',
                get_class($this)));
        }

        $this->value = $value;
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    public function getConstants(): array
    {
        $reflector = new ReflectionClass(get_called_class());
        $constants = $reflector->getConstants();
        $values = [];

        foreach ($constants as $constant => $value) {
            $values[] = $value;
        }

        return $values;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}