<?php declare(strict_types=1);

namespace App\DataSource\Types\Enum;

interface StringEnumInterface
{
    public function __construct(string $value);

    public function getValue(): string;
}