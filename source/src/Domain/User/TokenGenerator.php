<?php declare(strict_types=1);

namespace App\Domain\User;


class TokenGenerator
{
    /**
     * @return string
     */
    public function generateToken(): string
    {
        return md5(uuid_create(UUID_TYPE_TIME).uuid_create(UUID_TYPE_RANDOM));
    }
}