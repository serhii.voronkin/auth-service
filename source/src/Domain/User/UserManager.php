<?php declare(strict_types=1);

namespace App\Domain\User;

use App\DataSource\Entity\File\User;
use App\DataSource\Repository\UserRepository;
use App\Domain\User\Exception\UserNickNameNotUniqueException;
use App\Domain\User\Exception\UserTokenExpired;

class UserManager
{
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * UserManager constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string      $firstName
     * @param string      $lastName
     * @param string      $nickName
     * @param int         $age
     * @param string      $password
     * @param string|null $guestToken
     *
     * @return User
     * @throws UserNickNameNotUniqueException
     */
    public function register(
        string $firstName,
        string $lastName,
        string $nickName,
        int $age,
        string $password,
        ?string $guestToken
    ): User {

        if (!$this->userRepository->isNickNameUnique($nickName)) {
            throw new UserNickNameNotUniqueException('User login not unique',
                422);
        }

        if (!empty($guestToken)) {
            $user = $this->userRepository->getByToken($guestToken);
            $this->userRepository->updateUser($user, $firstName, $lastName,
                $nickName, $age, $password);

            return $user;
        }

        return $this->userRepository->create($firstName, $lastName, $nickName,
            $age, $password);
    }

    /**
     * @return User
     */
    public function registerGuest(): User
    {
        return $this->userRepository->createGuest();
    }
}


