<?php declare(strict_types=1);

namespace App\Presentation\Messenger\Message;

use DateTimeInterface;

class TrackingActionMessage
{
    /**
     * @var string
     */
    private string $userId;

    /**
     * @var string
     */
    private string $sourceType;

    /**
     * @var DateTimeInterface
     */
    private DateTimeInterface $dateCreated;

    /**
     * TrackingActionMessage constructor.
     *
     * @param string $userId
     * @param string $sourceType
     * @param DateTimeInterface $dateCreated
     */
    public function __construct(
        string $userId,
        string $sourceType,
        DateTimeInterface $dateCreated
    ) {
        $this->userId = $userId;
        $this->sourceType = $sourceType;
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getSourceType(): string
    {
        return $this->sourceType;
    }

    /**
     * @return DateTimeInterface
     */
    public function getDateCreated(): DateTimeInterface
    {
        return $this->dateCreated;
    }
}
