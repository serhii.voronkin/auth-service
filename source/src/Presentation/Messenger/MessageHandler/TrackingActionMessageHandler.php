<?php declare(strict_types=1);

namespace App\Presentation\Messenger\MessageHandler;

use App\Domain\TrackingAction\TrackingActionManager;
use App\Presentation\Messenger\Message\TrackingActionMessage;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class TrackingActionMessageHandler implements MessageHandlerInterface
{
    /**
     * @var TrackingActionManager
     */
    private TrackingActionManager $trackingActionManager;

    /**
     * TrackingActionMessageHandler constructor.
     *
     * @param TrackingActionManager $trackingActionManager
     */
    public function __construct(TrackingActionManager $trackingActionManager)
    {
        $this->trackingActionManager = $trackingActionManager;
    }

    /**
     * @param TrackingActionMessage $message
     *
     * @throws TransportExceptionInterface
     */
    public function __invoke(TrackingActionMessage $message): void
    {
        $this->trackingActionManager->trackAction(
            $message->getUserId(),
            $message->getSourceType(),
            $message->getDateCreated()
        );
    }
}
