<?php declare(strict_types=1);

namespace App\Presentation\Controller;

use App\Domain\User\Exception\UserNickNameNotUniqueException;
use App\Domain\User\Exception\UserTokenExpired;
use App\Domain\User\UserManager;
use App\Presentation\Controller\Request\UserRegister;
use App\Presentation\Controller\Traits\ResponseControllerTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController
{
    use ResponseControllerTrait;

    /**
     * @var UserManager
     */
    private UserManager $userManager;

    /**
     * RegisterController constructor.
     *
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @Route("/api/v1/user/register", methods={"POST"},  name="api.v1.user.register")
     * @SWG\Parameter(
     *         name="UserRegister",
     *         required=true,
     *         in="body",
     *         @Model(type=UserRegister::class),
     *         description="Parameter guestToken not required"
     *     )
     * @SWG\Response(
     *      response=201,
     *      description="Registration success",
     * )
     *
     *
     * @param UserRegister $userRegister
     *
     * @return JsonResponse
     * @throws UserTokenExpired
     * @throws UserNickNameNotUniqueException
     */
    public function registerAction(UserRegister $userRegister): JsonResponse
    {
        $this->userManager->register(
            $userRegister->getFirstName(),
            $userRegister->getLastName(),
            $userRegister->getNickName(),
            $userRegister->getAge(),
            $userRegister->getPassword(),
            $userRegister->getGuestToken()
        );

        return new JsonResponse(null, 201);
    }
}
