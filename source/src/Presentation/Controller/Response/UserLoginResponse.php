<?php declare(strict_types=1);

namespace App\Presentation\Controller\Response;

use App\Infrastructure\ServiceClient\DTO\LoginResponse;
use App\Presentation\Controller\Response\DTO\UserLoginDTO;

class UserLoginResponse extends ResponseItem
{
    /**
     * @var UserLoginDTO
     */
    protected $data;

    public function __construct(UserLoginDTO $response)
    {
        parent::__construct($response);
    }
}