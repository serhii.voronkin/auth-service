<?php declare(strict_types=1);

namespace App\Presentation\Controller\Response\DTO;


class UserLoginDTO
{
    private string $token;

    /**
     * UserLoginDTO constructor.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
}
