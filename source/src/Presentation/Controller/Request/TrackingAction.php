<?php declare(strict_types=1);

namespace App\Presentation\Controller\Request;

use DateTimeInterface;
use NaN\ApiBundle\Request\PostRequestInterface;
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\Constraints as Assert;

class TrackingAction implements PostRequestInterface
{
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Choice(callback={"App\DataSource\ValueObject\TrackingSourceTypeEnum", "getConstants"})
     */
    private string $sourceType;

    /**
     * @var DateTimeInterface
     * @Assert\NotBlank
     * @Assert\DateTime
     *
     * @SWG\Property(example="2020-12-01 11:11:11")
     */
    private DateTimeInterface $dateCreated;

    /**
     * TrackingAction constructor.
     *
     * @param string $sourceType
     * @param DateTimeInterface $dateCreated
     */
    public function __construct(
        string $sourceType,
        DateTimeInterface $dateCreated
    ) {
        $this->sourceType = $sourceType;
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return string
     */
    public function getSourceType(): string
    {
        return $this->sourceType;
    }

    /**
     * @return DateTimeInterface
     */
    public function getDateCreated(): DateTimeInterface
    {
        return $this->dateCreated;
    }
}
