<?php declare(strict_types=1);

namespace App\Presentation\Controller;

use App\Domain\User\Exception\UserTokenExpired;
use App\Domain\User\UserLogin as UserLoginManager;
use App\Domain\User\UserManager;
use App\Presentation\Controller\Request\UserLogin;
use App\Presentation\Controller\Response\DTO\UserLoginDTO;
use App\Presentation\Controller\Response\UserLoginResponse;
use App\Presentation\Controller\Traits\ResponseControllerTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{
    use ResponseControllerTrait;

    private UserManager $userManager;

    /**
     * @var UserLoginManager
     */
    private UserLoginManager $userLoginManager;

    /**
     * LoginController constructor.
     *
     * @param UserManager      $userManager
     * @param UserLoginManager $userLoginManager
     */
    public function __construct(
        UserManager $userManager,
        UserLoginManager $userLoginManager
    ) {
        $this->userManager = $userManager;
        $this->userLoginManager = $userLoginManager;
    }


    /**
     * @Route("/api/v1/user/login", methods={"POST"},  name="api.v1.user.login")
     * @SWG\Parameter(
     *         name="UserLogin",
     *         required=true,
     *         in="body",
     *         @Model(type=UserLogin::class)
     *     )
     * @SWG\Response(
     *      response=200,
     *      @Model(type=UserLoginResponse::class),
     *      description="Login success"
     * )
     *
     *
     * @param UserLogin $userLogin
     *
     * @return UserLoginResponse
     */
    public function loginAction(UserLogin $userLogin): UserLoginResponse
    {
        $authToken = $this->userLoginManager->login(
            $userLogin->getNickName(),
            $userLogin->getPassword()
        );

        return new UserLoginResponse(new UserLoginDTO($authToken));
    }

    /**
     * @Route("/api/v1/user/login-guest", methods={"POST"},  name="api.v1.user.login-gues")
     * @SWG\Response(
     *      response=200,
     *      @Model(type=UserLoginResponse::class),
     *      description="Login guest success"
     * )
     *
     *
     * @return UserLoginResponse
     */
    public function loginGuestAction(): UserLoginResponse
    {
        $user = $this->userManager->registerGuest();

        return new UserLoginResponse(new UserLoginDTO($user->getToken()));
    }

}
